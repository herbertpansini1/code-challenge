Digital Republic Code Challenge:

Instalação da Aplicação
Abra o terminal/Powershell e rode os comandos abaixo:
Clonando o repositório:
Via HTTPS:

$ git clone https://gitlab.com/herbertpansini1/code-challenge.git

$ cd code-challenge

$ docker-compose up -d

$ ./gradlew clean build -x test

$ ./gradlew bootRun


POST /
http://localhost:8080/sala

{
    "paredes": [
        {
            "largura": 6.00,
            "altura": 7.00,
            "quantidadePortas": 1,
            "quantidadeJanelas": 1
        },
        {
            "largura": 8.00,
            "altura": 5.00,
            "quantidadePortas": 2,
            "quantidadeJanelas": 2
        },
        {
            "largura": 5.00,
            "altura": 9.00,
            "quantidadePortas": 0,
            "quantidadeJanelas": 0
        },
        {
            "largura": 3.00,
            "altura": 8.00,
            "quantidadePortas": 0,
            "quantidadeJanelas": 1
        }
    ]
}
